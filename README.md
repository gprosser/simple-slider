A tiny L�VE library for customizable sliders that can manipulate properties, like audio volume.

![alt text](http://i.imgur.com/NUG3cbZ.gif "Volume slider")

![alt text](http://i.imgur.com/9p7rWRy.gif "Screenshake slider")

![alt text](http://i.imgur.com/cq77YlS.gif "Radius slider")


Example usage:
```lua
require 'simple-slider'

function love.load()
    music = love.audio.newSource('music.ogg')
    music:play()

    -- create a new slider
    volumeSlider = newSlider(400, 300, 300, 0.5, 0, 1, function (v) love.audio.setVolume(v) end)
end

function love.update(dt)
    -- update slider, must be called every frame
    volumeSlider:update()
end

function love.draw()
    love.graphics.setBackgroundColor(249, 205, 173)

    love.graphics.setLineWidth(4)
    love.graphics.setColor(254, 67, 101)

    -- draw slider, set color and line style before calling
    volumeSlider:draw()
end
```