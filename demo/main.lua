require 'simple-slider'

function love.load()
    -- music
    music = love.audio.newSource('music.ogg')
    music:setLooping(true)
    music:play()

    -- fonts
    font1 = love.graphics.newFont('rainyhearts.ttf', 48)
    font2 = love.graphics.newFont('retro-computer.ttf', 28)
    font3 = love.graphics.newFont('pixelmix.ttf', 24)

    -- properties
    screenshake = 0.5
    red = 128
    green = 0
    blue = 64

    -- sliders
    volumeSlider = newSlider(500, 90, 150, 0.5, 0, 1, function (v) love.audio.setVolume(v) end, {width=28, orientation='horizontal', track='rectangle', knob='rectangle'})

    screenshakeSlider = newSlider(400, 310, 300, screenshake, 0.5, 2, function (v) screenshake = v end, {width=20, orientation='horizontal', track='line', knob='rectangle'})

    redSlider = newSlider(330, 478, 90, red, 0, 255, function (v) red=v end, {width=15, orientation='vertical', track='roundrect', knob='circle'})
    greenSlider = newSlider(400, 478, 90, green, 0, 255, function (v) green=v end, {width=15, orientation='vertical', track='line', knob='circle'})
    blueSlider = newSlider(470, 478, 90, blue, 0, 255, function (v) blue=v end, {width=15, orientation='vertical', track='rectangle', knob='rectangle'})
end

function love.update(dt)
    -- update sliders
    volumeSlider:update()

    screenshakeSlider:update()

    redSlider:update()
    greenSlider:update()
    blueSlider:update()
end

function love.draw()
    -- #1: volume
    love.graphics.setColor(249, 205, 173)
    love.graphics.rectangle('fill', 0, 0, 800, 200)

    love.graphics.setLineWidth(3)
    love.graphics.setLineStyle('rough')
    love.graphics.setColor(254, 67, 101)
    love.graphics.setFont(font1)
    love.graphics.print('volume', 220, 60)
    volumeSlider:draw()

    -- #2: screenshake
    love.graphics.setColor(136, 192, 112)
    love.graphics.rectangle('fill', 0, 200, 800, 200)

    love.graphics.setColor(48, 104, 80)
    love.graphics.setFont(font2)
    love.graphics.print('screenshake', 220 + math.sin(2 * math.pi * 14 * love.timer.getTime()) * 4 * screenshake, 240)
    love.graphics.print(math.floor(screenshakeSlider:getValue() * 100 + 0.5) .. '%', 540 + math.sin(2 * math.pi * 14 * love.timer.getTime()) * 4 * screenshake, 240)
    screenshakeSlider:draw()

    -- #3: rgb
    love.graphics.setColor(red, green, blue)
    love.graphics.rectangle('fill', 0, 400, 800, 200)

    love.graphics.setColor(255, 255, 255)
    love.graphics.setFont(font3)

    love.graphics.print('R', 324, 540)
    redSlider:draw()

    love.graphics.print('G', 394, 540)
    greenSlider:draw()

    love.graphics.print('B', 464, 540)
    blueSlider:draw()
end